// imports liberries for making components
import React from 'react';
import { Text, View } from 'react-native';


//make components

const Header = (props) => {
      const { textStyle, viewStyle } = styles;
          return (
          <View style={viewStyle}>
          <Text style={textStyle}> {props.headerText}</Text>
          </View>
          );
};

const styles = {
   viewStyle: {
   backgroundColor: '#E0E0E0',
   justifyContent: 'center',
   alignItems: 'center',
   height: 80,
   paddingTop: 15,
//    shadowColor: '#000',
//    shadowOffset: { width: 0, height: 2 },
//    shadowOpacity: 0.2,
   //elevation: 2,
   //position: 'relative'

   }, 
   textStyle: {
      fontSize: 30
   }
};
//make the components available for the other parts
export default Header;
