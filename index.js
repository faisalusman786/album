/** @format */

// import { AppRegistry } from 'react-native';
// import App from './App';
// import { name as appName } from './app.json';

// AppRegistry.registerComponent(appName, () => App);

// import liberries for creating a component
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';

//create a component
const App = () => 
   (
     //<Text> Some Text </Text>
     <View style={{ flex: 1 }}>
     <Header headerText={'Album'} />
     <AlbumList />
     </View>
  );


//render that component on a screen

AppRegistry.registerComponent('albums', () => App);
